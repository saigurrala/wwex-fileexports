package com.wwex.reports.service.email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Encapsulates all information received as a response to an {@link Email}.
 * @author blake
 * @since 4/29/15
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailResponse {

    private String id;
    private String message;
    private Integer status;

    /**
     * Default constructor.
     */
    public EmailResponse() {
    }

    /**
     * Constructor.
     * @param id The identifier of the response.
     * @param message The message of the response.
     * @param status The status of the response i.e. 200 on success.
     */
    public EmailResponse(String id, String message, Integer status) {
        this.id = id;
        this.message = message;
        this.status = status;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
}
