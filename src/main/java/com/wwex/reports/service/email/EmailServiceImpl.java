package com.wwex.reports.service.email;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.wwex.reports.model.EmailData;
import com.wwex.reports.model.InsuranceType;

@Service
public class EmailServiceImpl implements EmailService {
	static final Logger logger = Logger.getLogger(EmailServiceImpl.class);

	@Autowired
	@Qualifier("jdbcWexnet")
	private JdbcTemplate jdbcWexnet;

	@Value("${spring.datasource.wexnet.schema}")
	private String wexnetSchema;

	@Value("${email.mailgun.apiKey}")
	private String apiKey;

	@Value("${email.mailgun.host}")
	private String host;

	@Value("${email.mailgun.baseUrl}")
	private String baseUrl;

	@Value("${email.marshclaimsfailure.from}")
	private String failureEmailFrom;

	@Value("${email.marshclaimsfailure.to}")
	private String failureEmailTo;

	@Value("${email.marshclaims.cc}")
	private String claimsEmailCC;

	@Override
	public EmailData populateEmailData(EmailData emailData) {
		if (InsuranceType.MARSHINSURANCEFAILURE.toString().equalsIgnoreCase(emailData.getType())) {
			return populateMarshInsFailureEmailData(emailData);
		}
		return null;
	}

	private EmailData populateMarshInsFailureEmailData(EmailData emailData) {

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy  hh:mm:ss a");
		emailData.setEmailTo(failureEmailTo);
		emailData.setEmailSubject("Error in generating Marsh Insurance CLaims Export: " + sdf.format(new Date()));
		emailData.setEmailFrom(failureEmailFrom);
		emailData.setEmailCc(claimsEmailCC);
		return emailData;
	}

	@Override
	public EmailResponse send(EmailData email) throws EmailException {
		final String ENDPOINT = "/messages";
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(MultiPartFeature.class);
		Client client = ClientBuilder.newClient(clientConfig);
		client.register(HttpAuthenticationFeature.basic("api", apiKey));
		WebTarget target = client.target(baseUrl + host).path(ENDPOINT);

		FormDataMultiPart part = new FormDataMultiPart();
		part.field("from", email.getEmailFrom());
		if (email.getEmailTo() != null)
			part.field("to", email.getEmailTo());
		if (email.getEmailCc() != null)
			part.field("cc", email.getEmailCc());
		StringBuilder sb = new StringBuilder();
		/*
		 * if (environment != null) { sb.append("[")
		 * .append(environment.toUpperCase()) .append("] "); }
		 */
		sb.append(email.getEmailSubject());
		part.field("subject", sb.toString());
		part.field("html", email.getEmailBody());

		// check if email is queued
		/*
		 * if(email.getDeliveryTime() != null) part.field("o:deliverytime",
		 * email.getDeliveryTime());
		 */

		// InCase we have (an) attachment(s)
		if (email.getAttachment() != null) {
			// get files from email and process as form component
			// for (Object thisFile : email.getAttachment()) {
			FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("attachment", (File) email.getAttachment(),
					MediaType.APPLICATION_OCTET_STREAM_TYPE);
			part.bodyPart(fileDataBodyPart);
			// }
		}

		// Invoke Service
		Invocation.Builder invocationBuilder = target.request();
		Response response = invocationBuilder.post(Entity.entity(part, "multipart/form-data"), Response.class);
		return getEmailResponse(response);
	}

	private EmailResponse getEmailResponse(Response response) throws EmailException {
		if (response == null) {
			throw new EmailException("'response' is null");
		}

		EmailResponse emailResponse = response.readEntity(EmailResponse.class);
		if (response.getStatus() >= 400) {
			logger.error(String.format("There was an error: %s", response.getStatus()));
			throw new EmailException(
					String.format("We received an error response from MailGun - HTTP[%s].", response.getStatus()));
		}
		if (emailResponse == null) {
			logger.error("The email response could not be de-serialized");
			throw new EmailException("Could not be de-serialize the email response.");
		}
		emailResponse.setStatus(response.getStatus());

		return emailResponse;
	}

	@Override
	public EmailResponse sendFailure(String failureType, String body) throws EmailException {
		EmailData email = new EmailData();
		email.setType(failureType);
		email.setEmailBody(body);
		populateEmailData(email);

		return send(email);
	}

}
