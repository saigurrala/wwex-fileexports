package com.wwex.reports.service.email;

import com.wwex.reports.model.EmailData;

public interface EmailService {

	EmailResponse send(EmailData email) throws EmailException;

	EmailData populateEmailData(EmailData emailData);
	
	EmailResponse sendFailure(String failureType, String body) throws EmailException;


}
