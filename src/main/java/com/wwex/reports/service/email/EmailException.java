package com.wwex.reports.service.email;

/**
 * Generic exception to be thrown when there are email issues.
 *
 * @author blake
 * @since 4/29/15
 */
public class EmailException extends Exception {

    /**
     * {@inheritDoc}
     */
    public EmailException(String message) {
        super(message);
    }
}
