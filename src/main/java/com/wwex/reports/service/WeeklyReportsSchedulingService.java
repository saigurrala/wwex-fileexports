package com.wwex.reports.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wwex.reports.billingops2.MarshInsuranceClaims;
import com.wwex.reports.service.email.EmailException;

@Service
public class WeeklyReportsSchedulingService {

	static final Logger logger = Logger.getLogger(WeeklyReportsSchedulingService.class);

	@Autowired
	private MarshInsuranceClaims marshInsuranceClaims;

	@Value("${marsh.scheduling.job.cron}")
	private String MarshInsuranceCronJob;

	@Scheduled(cron = "${marsh.scheduling.job.cron}", zone = "CST")
	public void generateMarshClaimExport() {

		logger.info("Started Marsh claim Export cron job: " + MarshInsuranceCronJob);

		try {
			marshInsuranceClaims.generateMarshClaimReport();
		} catch (EmailException e) {
			e.printStackTrace();
			logger.error(e);
		}

	}
}
