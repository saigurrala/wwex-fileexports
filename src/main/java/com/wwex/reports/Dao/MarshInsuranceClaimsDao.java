package com.wwex.reports.Dao;

import java.util.List;
import java.util.Map;

import com.wwex.reports.model.MarshInsurance;
import com.wwex.reports.model.MarshInsuranceTypeData;
import com.wwex.reports.service.email.EmailException;

public interface MarshInsuranceClaimsDao {

	public Map<String, MarshInsurance> getNewClaims() throws EmailException;

	public Map<String, MarshInsurance> getInsuranceDetails(Map<String, MarshInsurance> claims) throws EmailException;

	public void updateClaimDetails(Map<String, MarshInsurance> insDetails) throws EmailException;

	public List<MarshInsuranceTypeData> getInsuranceAndDescrepencydData() throws EmailException;

	public void updateClaimStatus() throws EmailException;
}
