package com.wwex.reports.Dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.wwex.reports.model.InsuranceType;
import com.wwex.reports.model.MarshInsurance;
import com.wwex.reports.model.MarshInsuranceTypeData;
import com.wwex.reports.service.email.EmailException;
import com.wwex.reports.service.email.EmailService;

@Service
public class MarshInsuranceClaimsDaoImpl implements MarshInsuranceClaimsDao {

	static final Logger logger = Logger.getLogger(MarshInsuranceClaimsDaoImpl.class);

	@Autowired
	@Qualifier("jdbcWexnet")
	private JdbcTemplate jdbcWexnet;

	@Autowired
	private EmailService emailService;

	@Transactional
	public Map<String, MarshInsurance> getNewClaims() throws EmailException {
		Map<String, MarshInsurance> marshNewClaimDetails = new HashMap<String, MarshInsurance>();
		try {
			logger.info("Generating new claims");
			List<Map<String, Object>> results = jdbcWexnet
					.queryForList("select ID, AIRBILL_NO from wexnet.ins_cargo_claims where status=?", "NEWCLAIM");
			if (!CollectionUtils.isEmpty(results)) {
				for (Map<String, Object> row : results) {
					if (row.get("ID") != null) {
						MarshInsurance marshInsurance = new MarshInsurance();
						marshInsurance.setId((BigDecimal) row.get("ID"));
						marshInsurance.setAirBillNo((String) row.get("AIRBILL_NO"));
						marshNewClaimDetails.put(((BigDecimal) row.get("ID")).toString(), marshInsurance);
						logger.info("ID = " + marshInsurance.getId() + " AirBillNum= " + marshInsurance.getAirBillNo());
					}
				}
			} else {
				logger.info("There are no new claims at this point.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			marshNewClaimDetails = null;
			logger.error("Error in getting new claims for Marsh Insurance." + e);
			emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
					"Error in getting new claims for Marsh Insurance.");
		}
		return marshNewClaimDetails;
	}

	@Transactional
	public Map<String, MarshInsurance> getInsuranceDetails(Map<String, MarshInsurance> claims) throws EmailException {

		Map<String, MarshInsurance> claimsUpdated = new HashMap<String, MarshInsurance>();

		if (!claims.isEmpty()) {
			try {

				String ins = "select" + " TO_NUMBER(ADDITIONALPROTECTIONVALUE) INSURANCE_VALUE , "
						+ " wexnet.fn_get_insurance_premium(CHARGETYPECODE,CHARGETYPEDESC) INSURANCE_PREMIUM, "
						+ " SHIPDATE AIRBILL_DATE, "
						+ " DECODE(CODPAYMENTCODE,0,'check cashiers check or money order - no cash allowed',8,'cashiers check or money order - no cash allowed.',CODPAYMENTCODE) COD_PAYMENT_TYPE, "
						+ " TO_NUMBER(CODPAYMENTVALUE) COD_VALUE " + " from wwex.shipmenthistory "
						+ " where AIRBILLNBR = ? " + " union all " + "	select "
						+ "  l.INSURANCE_VALUE as INSURANCE_VALUE,  "
						+ "  c.SHIPMENT_CHARGE_VALUE as INSURANCE_PREMIUM, " + "  l.CREATION_DATE as AIRBILL_DATE, "
						+ "  DECODE(l.COD,0,'check cashiers check or money order - no cash allowed',8,'cashiers check or money order - no cash allowed.',l.COD) as COD_PAYMENT_TYPE, "
						+ "  l.COD_VALUE as COD_VALUE" + " from s3f.shipment_lots l, s3f.shipment_charges c "
						+ " where l.SHIPMENT_LOT_ID = c.SHIPMENT_LOT_ID and "
						+ "     c.SHIPMENT_CHARGE_CODE = 'INS' and " + "  l.marsh_batch_id is not null and "
						+ "  l.TRACKING_NBR = ?";

				logger.info("Generating insurance details using the query " + ins);
				for (MarshInsurance mi : claims.values()) {
					String paddedAirBillNo = mi.getAirBillNo();
					if (!StringUtils.isEmpty(paddedAirBillNo) && paddedAirBillNo.length() < 10)
						paddedAirBillNo = "0000000000".substring(mi.getAirBillNo().length()) + mi.getAirBillNo();
					List<Map<String, Object>> results = jdbcWexnet.queryForList(ins, paddedAirBillNo, paddedAirBillNo);
					if (!CollectionUtils.isEmpty(results)) {
						for (Map<String, Object> row : results) {
							if (null != row.get("COD_PAYMENT_TYPE"))
								mi.setCopaymentType((String) row.get("COD_PAYMENT_TYPE"));
							

							if (null != row.get("COD_VALUE") && !"".equals(row.get("COD_VALUE"))) {
								if (((BigDecimal) row.get("COD_VALUE")) == (new BigDecimal("775509717"))) {
									mi.setCopaymentValue(new BigDecimal(0));
								} else {
									mi.setCopaymentValue((BigDecimal) row.get("COD_VALUE"));
								}
							} else
								mi.setCopaymentValue(new BigDecimal(0));

							if (null != row.get("INSURANCE_PREMIUM"))
								mi.setInsurancePremium((BigDecimal) row.get("INSURANCE_PREMIUM"));
							else
								mi.setInsurancePremium(new BigDecimal(0));

							if (null != row.get("INSURANCE_VALUE"))
								mi.setInsuranceValue((BigDecimal) row.get("INSURANCE_VALUE"));
							else
								mi.setInsuranceValue(new BigDecimal(0));

							if (null != row.get("AIRBILL_DATE"))
								mi.setAirBillDate((Timestamp) row.get("AIRBILL_DATE"));
						}
					} else {
						mi.setCopaymentValue(new BigDecimal(0));
						mi.setInsurancePremium(new BigDecimal(0));
						mi.setInsuranceValue(new BigDecimal(0));
						mi.setCopaymentType("N/A");
					}
					logger.info("Insurance Claims updated for " + mi.getId() + " with padded AirBilNo="
							+ paddedAirBillNo + " COD_PAYMENT_TYPE=" + mi.getCopaymentType() + " COD_VALUE="
							+ mi.getCopaymentValue() + " INSURANCE_PREMIUM=" + mi.getInsurancePremium()
							+ " INSURANCE_VALUE=" + mi.getInsuranceValue() + " AIRBILL_DATE=" + mi.getAirBillDate());
					claimsUpdated.put(mi.getId().toString(), mi);
				}
			} catch (Exception e) {
				e.printStackTrace();
				claimsUpdated = null;
				logger.error("Error in getting insurance details for new claims " + e);
				emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
						"Error in getting insurance details for new claims");
			}
		}
		return claimsUpdated;
	}

	public void updateClaimDetails(Map<String, MarshInsurance> insDetails) throws EmailException {
		try {
			String updateQ = "Update " + " WexNet.INS_CARGO_CLAIMS  " + " SET  " + " 	STATUS = 'PENDINGEXPORT', "
					+ " 	INSURANCE_VALUE = ?,  " + " 	INSURANCE_PREMIUM = ?,  " + " 	COD_PAYMENT_TYPE = ?,  "
					+ " 	COD_VALUE = ?,  " + " ACTUAL_SHIPMENT_DATE = to_date( ?" + ",'mm/dd/yyyy') "
					+ " where ID = ?";			
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			if (!insDetails.isEmpty()) {
				logger.info("Started updating insurance details for new claims using query " + updateQ);
				for (MarshInsurance mi : insDetails.values()) {
					String actualShipmentDateString = null;
					if (null != mi.getAirBillDate()) {
						actualShipmentDateString = sdf.format(new Date(mi.getAirBillDate().getTime()));
					}
					jdbcWexnet.update(updateQ, mi.getInsuranceValue(), mi.getInsurancePremium(), mi.getCopaymentType(),
							mi.getCopaymentValue(), actualShipmentDateString, mi.getId());
					logger.info("Updated insurance claims for id " + mi.getId() + " with insurance value="
							+ mi.getInsuranceValue() + " ins prem= " + mi.getInsurancePremium() + " payment type= "
							+ mi.getCopaymentType() + " CodVal=" + mi.getCopaymentValue() + " actualShipDate="
							+ actualShipmentDateString);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in updaing insurance details for new claims" + e);
			emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
					"Error in updaing insurance details for new claims");
		}
	}

	public List<MarshInsuranceTypeData> getInsuranceAndDescrepencydData() throws EmailException {

		List<MarshInsuranceTypeData> marshInsuranceTypeList = new ArrayList<MarshInsuranceTypeData>();
		try {
			String insClaims = "Select * From WexNet.VW_INS_CARGO_CLAIMS Order By ID";
			logger.info("Getting complete insurance details with query " + insClaims);
			String descrepancyData = "Select * From WexNet.VW_INS_CARGO_CLAIMS where (INSURANCE_VALUE = 0 or INSURANCE_PREMIUM = 0) Order By ID";
			logger.info("Getting descrepency details with query " + descrepancyData);
			List<Map<String, Object>> insClaimsResults = jdbcWexnet.queryForList(insClaims);
			List<Map<String, Object>> descrepencyResults = jdbcWexnet.queryForList(descrepancyData);
			if (!CollectionUtils.isEmpty(insClaimsResults)) {
				MarshInsuranceTypeData marshInsuranceType = new MarshInsuranceTypeData();
				List<MarshInsurance> insClaimsData = getInsuranceDetailsForExcelData(insClaimsResults);
				marshInsuranceType.setType(InsuranceType.INSURANCECLAIMS.toString());
				marshInsuranceType.setMarshInsuranceList(insClaimsData);
				marshInsuranceTypeList.add(marshInsuranceType);
			} else {
				logger.info("There are no insurance claims to populate.");
			}
			if (!CollectionUtils.isEmpty(descrepencyResults)) {
				MarshInsuranceTypeData marshInsuranceType = new MarshInsuranceTypeData();
				List<MarshInsurance> descrepencyData = getInsuranceDetailsForExcelData(descrepencyResults);
				marshInsuranceType.setType(InsuranceType.DESCREPENCYCLAIMS.toString());
				marshInsuranceType.setMarshInsuranceList(descrepencyData);
				marshInsuranceTypeList.add(marshInsuranceType);
			} else {
				logger.info("There are no descrepency claims to populate.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			marshInsuranceTypeList = null;
			logger.error("Error in getting insurance and descrepency details for updated claims" + e);
			emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
					"Error in getting insurance and descrepency details for updated claims");
		}
		return marshInsuranceTypeList;
	}

	public List<MarshInsurance> getInsuranceDetailsForExcelData(List<Map<String, Object>> insClaimsResults)
			throws EmailException {
		List<MarshInsurance> insList = new ArrayList<MarshInsurance>();
		try {
			if (!CollectionUtils.isEmpty(insClaimsResults)) {
				for (Map<String, Object> row : insClaimsResults) {
					MarshInsurance marIns = new MarshInsurance();
					marIns.setId((BigDecimal) row.get("ID"));
					marIns.setAirBillNo((String) row.get("AIRBILL_NO"));
					marIns.setFiledWithUps((String) row.get("FILED_WITH_UPS"));
					marIns.setCheckPayableTo((String) row.get("CHECK_PAYABLE_TO"));
					marIns.setCheckSentTo((String) row.get("CHECK_SENT_TO"));
					marIns.setCheckSendToOther((String) row.get("CHECK_SEND_TO_OTHER"));
					marIns.setFranchiseEmail((String) row.get("EMAIL_ADDRESS"));
					marIns.setFranchiseAddress((String) row.get("FRANCHISE_ADDRESS"));
					marIns.setFranchiseNumber((String) row.get("FRANCHISE_NUMBER"));
					marIns.setFranchiseLocation((String) row.get("FRANCHISE_LOCATION"));
					marIns.setFranchiseCity((String) row.get("FRANCHISE_CITY"));
					marIns.setFranchiseState((String) row.get("FRANCHISE_STATE"));
					marIns.setFranchiseZipCode((String) row.get("FRANCHISE_ZIP"));
					marIns.setShipperCompanyName((String) row.get("SHIPPER_COMPANY"));
					marIns.setShipperAddress((String) row.get("SHIPPER_ADDRESS"));
					marIns.setShipperContactName((String) row.get("SHIPPER_CONTACT"));
					marIns.setShipperCity((String) row.get("SHIPPER_CITY"));
					marIns.setShipperState((String) row.get("SHIPPER_STATE"));
					marIns.setShipperZipCode((String) row.get("SHIPPER_ZIP"));
					marIns.setShipperTelephone((String) row.get("SHIPPER_PHONE"));
					marIns.setReceiverAddress((String) row.get("RECEIVER_ADDRESS"));
					marIns.setReceiverCity((String) row.get("RECEIVER_CITY"));
					marIns.setReceiverCompanyName((String) row.get("RECEIVER_COMPANY"));
					marIns.setReceiverContactName((String) row.get("RECEIVER_CONTACT"));
					marIns.setReceiverstate((String) row.get("RECEIVER_STATE"));
					marIns.setReceiverTelephone((String) row.get("RECEIVER_PHONE"));
					marIns.setReceiverZipCode((String) row.get("RECEIVER_ZIP"));
					marIns.setDateOfShipment((Timestamp) row.get("SHIPMENT_DATE"));
					marIns.setClaimAmount((BigDecimal) row.get("INSURED_AMOUNT"));
					marIns.setClaimType((String) row.get("CLAIM_TYPE"));
					marIns.setProductAddress((String) (String) row.get("PRODUCT_ADDRESS"));
					marIns.setProductCity((String) (String) row.get("PRODUCT_CITY"));
					marIns.setProductCompanyName((String) row.get("PRODUCT_COMPANY"));
					marIns.setProductContactName((String) row.get("PRODUCT_CONTACT"));
					marIns.setProductLocation((String) row.get("PRODUCT_LOCATION"));
					marIns.setProductState((String) row.get("PRODUCT_STATE"));
					marIns.setProductTelephone((String) row.get("PRODUCT_PHONE"));
					marIns.setProductZipCode((String) row.get("PRODUCT_ZIP"));
					marIns.setDescriptionIemOne((String) row.get("DESCRIPTION1"));
					marIns.setDescriptionItemOneValue((BigDecimal) row.get("VALUE1"));
					marIns.setDescriptionItemTwo((String) row.get("DESCRIPTION2"));
					marIns.setDescriptionItemTwoValue((BigDecimal) row.get("VALUE2"));
					marIns.setDescriptionItemThree((String) row.get("DESCRIPTION3"));
					marIns.setDescriptionItemThreeValue((BigDecimal) row.get("VALUE3"));
					marIns.setTotalAmountClaimed((BigDecimal) row.get("TOTAL_CLAIM_VALUE"));
					marIns.setClaimFiledBy((String) row.get("FILED_BY"));
					marIns.setClaimDateReported((Timestamp) row.get("CLAIM_SUBMISSION_DATE"));
					marIns.setClaimantTelephone((String) row.get("FILED_BY_PHONE"));
					marIns.setInsuranceValue((BigDecimal) row.get("INSURANCE_VALUE"));
					marIns.setInsurancePremium((BigDecimal) row.get("INSURANCE_PREMIUM"));
					marIns.setCopaymentType((String) row.get("COD_PAYMENT_TYPE"));
					marIns.setCopaymentValue((BigDecimal) row.get("COD_VALUE"));
					marIns.setActualDateOfShipment((Timestamp) row.get("ACTUAL_SHIPMENT_DATE"));
					marIns.setUpsClaimNum((String) row.get("UPS_CLAIM_NO"));
					insList.add(marIns);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			insList = null;
			logger.error("Error in generating complete insurance details for updated claims" + e);
			emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
					"Error in generating complete insurance details for updated claims");
		}
		return insList;
	}

	public void updateClaimStatus() throws EmailException {
		try {
			logger.info("Started updating claim status to exported for pendingexport claims");
			String query = "Update WexNet.INS_CARGO_CLAIMS SET STATUS = 'EXPORTED', EXPORT_DATE = systimestamp where STATUS = 'PENDINGEXPORT'";
			jdbcWexnet.update(query);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in updaing claim status to exported for pendingexport claims" + e);
			emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
					"Error in updaing claim status to exported for pendingexport claims");
		}
	}

}
