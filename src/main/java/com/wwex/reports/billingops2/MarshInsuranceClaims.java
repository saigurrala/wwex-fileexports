package com.wwex.reports.billingops2;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.wwex.reports.Dao.MarshInsuranceClaimsDao;
import com.wwex.reports.files.ExcelFileGenerationService;
import com.wwex.reports.model.EmailData;
import com.wwex.reports.model.InsuranceType;
import com.wwex.reports.model.MarshInsurance;
import com.wwex.reports.model.MarshInsuranceTypeData;
import com.wwex.reports.service.email.EmailException;
import com.wwex.reports.service.email.EmailService;

@Service
public class MarshInsuranceClaims {

	static final Logger logger = Logger.getLogger(MarshInsuranceClaims.class);

	@Autowired
	MarshInsuranceClaimsDao marshInsClaimsDao;

	@Autowired
	ExcelFileGenerationService excelFileGenerationService;

	@Autowired
	private EmailService emailService;

	@Value("${email.insuranceclaims.from}")
	private String insuranceClaimsEmailFrom;

	@Value("${email.insuranceclaims.to}")
	private String insuranceClaimsEmailTo;

	@Value("${email.descrepencyclaims.from}")
	private String desClaimsEmailFrom;

	@Value("${email.descrepencyclaims.to}")
	private String desClaimsEmailTo;

	@Value("${email.marshclaims.cc}")
	private String claimsEmailCC;

	public void generateMarshClaimReport() throws EmailException {
		EmailData emailData = null;
		try {
			Map<String, MarshInsurance> claims = marshInsClaimsDao.getNewClaims();

			Map<String, MarshInsurance> insDetails = marshInsClaimsDao.getInsuranceDetails(claims);

			marshInsClaimsDao.updateClaimDetails(insDetails);

			List<MarshInsuranceTypeData> marshInsuranceList = marshInsClaimsDao.getInsuranceAndDescrepencydData();

			excelFileGenerationService.generateMarshInsuranceFile(marshInsuranceList);

			boolean update = true;

			if (!CollectionUtils.isEmpty(marshInsuranceList)) {
				for (MarshInsuranceTypeData insType : marshInsuranceList) {
					emailData = new EmailData();
					emailData.setType(insType.getType());
					emailData = populateMarshInsEmailData(emailData);
					if (null != emailData && null != insType.getMarshFile()) {
						emailData.setAttachment(insType.getMarshFile());
						emailService.send(emailData);
					} else {
						update = false;
						logger.info("There are some issues in populating email data or marsh insurance file.");
					}
				}
				if (update) {
					marshInsClaimsDao.updateClaimStatus();
					logger.info("Successfully udated Claims status to Exported");
				}
			}

		} catch (EmailException e) {
			e.printStackTrace();
			logger.error("Error in generating marsh claims export." + e);
			emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
					"Error in generating marsh claims export.");
		}

	}

	private EmailData populateMarshInsEmailData(EmailData emailData) {

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy  hh:mm:ss a");
		if (InsuranceType.INSURANCECLAIMS.toString().equalsIgnoreCase(emailData.getType())) {
			emailData.setEmailTo(insuranceClaimsEmailTo);
			emailData.setEmailSubject("Insurance Claims File: " + sdf.format(new Date()));
			emailData.setEmailBody("Insurance File");
			emailData.setEmailFrom(insuranceClaimsEmailFrom);
			emailData.setEmailCc(claimsEmailCC);
		} else if (InsuranceType.DESCREPENCYCLAIMS.toString().equalsIgnoreCase(emailData.getType())) {
			emailData.setEmailTo(desClaimsEmailTo);
			emailData.setEmailSubject("Insurance Claims Discrepancy File:" + sdf.format(new Date()));
			emailData.setEmailBody("Discrepancy File");
			emailData.setEmailFrom(desClaimsEmailFrom);
			emailData.setEmailCc(claimsEmailCC);
		}
		return emailData;
	}

}
