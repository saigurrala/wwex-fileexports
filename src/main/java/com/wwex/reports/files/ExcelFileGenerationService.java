package com.wwex.reports.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.wwex.reports.model.InsuranceType;
import com.wwex.reports.model.MarshInsurance;
import com.wwex.reports.model.MarshInsuranceTypeData;
import com.wwex.reports.service.email.EmailException;
import com.wwex.reports.service.email.EmailService;

@Service
public class ExcelFileGenerationService {

	@Autowired
	private EmailService emailService;

	static final Logger logger = Logger.getLogger(ExcelFileGenerationService.class);

	public List<MarshInsuranceTypeData> generateMarshInsuranceFile(List<MarshInsuranceTypeData> marshInsuranceList)
			throws EmailException {

		String date = new SimpleDateFormat("YYYYMMdd_hhmm").format(new Date());
		FileInputStream file = null;
		File genInsClaimsFile = null, genDescClaimsFile = null, templateInsClaimsFile = null,
				templateDescClaimsFile = null, excelInsPopFile = null, excelDescPopFile = null;

		if (!CollectionUtils.isEmpty(marshInsuranceList)) {
			for (MarshInsuranceTypeData marshInsType : marshInsuranceList) {

				try {
					if (InsuranceType.INSURANCECLAIMS.toString().equalsIgnoreCase(marshInsType.getType())) {
						if (genInsClaimsFile == null && templateInsClaimsFile == null) {
							FileUtils.cleanDirectory(new File("temp/InsuranceClaims"));
							templateInsClaimsFile = new File("ExcelTemplates/InsuranceClaims.xls");
							genInsClaimsFile = new File(
									"temp/InsuranceClaims/InsuranceClaims_" + date.toString() + ".xls");
							FileUtils.copyFile(templateInsClaimsFile, genInsClaimsFile);
							file = new FileInputStream(genInsClaimsFile);
						}
						logger.info("Populating " + marshInsType.getType() + " to excel file "
								+ genInsClaimsFile.getName());
						excelInsPopFile = populateDataToExcel(file, genInsClaimsFile,
								marshInsType.getMarshInsuranceList());
						marshInsType.setMarshFile(excelInsPopFile);
					}
					if (excelInsPopFile != null
							&& InsuranceType.DESCREPENCYCLAIMS.toString().equalsIgnoreCase(marshInsType.getType())) {
						if (genDescClaimsFile == null && templateDescClaimsFile == null) {
							FileUtils.cleanDirectory(new File("temp/DiscrepancyClaims"));
							templateDescClaimsFile = new File("ExcelTemplates/Discrepancy.xls");
							genDescClaimsFile = new File(
									"temp/DiscrepancyClaims/Discrepancy_" + date.toString() + ".xls");
							FileUtils.copyFile(templateDescClaimsFile, genDescClaimsFile);
							file = new FileInputStream(genDescClaimsFile);

						}
						logger.info("Populating " + marshInsType.getType() + " to excel file "
								+ genDescClaimsFile.getName());
						excelDescPopFile = populateDataToExcel(file, genDescClaimsFile,
								marshInsType.getMarshInsuranceList());
						marshInsType.setMarshFile(excelDescPopFile);
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Error in generating insurance details to Excel file." + e);
					emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
							"Error in generating insurance details to Excel file.");
				}
			}

		}
		return marshInsuranceList;
	}

	public File populateDataToExcel(FileInputStream file, File genFile, List<MarshInsurance> excelFileData)
			throws IOException {
		HSSFWorkbook book = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy  hh:mm:ss a");
			book = new HSSFWorkbook(file);
			HSSFSheet sheet0 = book.getSheetAt(0);
			int i = 1, j = -1;
			if (!CollectionUtils.isEmpty(excelFileData)) {
				for (MarshInsurance rec : excelFileData) {
					try {
						Cell cell0 = sheet0.getRow(i).createCell(++j);
						Cell cell1 = sheet0.getRow(i).createCell(++j);
						Cell cell2 = sheet0.getRow(i).createCell(++j);
						Cell cell3 = sheet0.getRow(i).createCell(++j);
						Cell cell4 = sheet0.getRow(i).createCell(++j);
						Cell cell5 = sheet0.getRow(i).createCell(++j);
						Cell cell6 = sheet0.getRow(i).createCell(++j);
						Cell cell7 = sheet0.getRow(i).createCell(++j);
						Cell cell8 = sheet0.getRow(i).createCell(++j);
						Cell cell9 = sheet0.getRow(i).createCell(++j);
						Cell cell10 = sheet0.getRow(i).createCell(++j);
						Cell cell11 = sheet0.getRow(i).createCell(++j);
						Cell cell12 = sheet0.getRow(i).createCell(++j);
						Cell cell13 = sheet0.getRow(i).createCell(++j);
						Cell cell14 = sheet0.getRow(i).createCell(++j);
						Cell cell15 = sheet0.getRow(i).createCell(++j);
						Cell cell16 = sheet0.getRow(i).createCell(++j);
						Cell cell17 = sheet0.getRow(i).createCell(++j);
						Cell cell18 = sheet0.getRow(i).createCell(++j);
						Cell cell19 = sheet0.getRow(i).createCell(++j);
						Cell cell20 = sheet0.getRow(i).createCell(++j);
						Cell cell21 = sheet0.getRow(i).createCell(++j);
						Cell cell22 = sheet0.getRow(i).createCell(++j);
						Cell cell23 = sheet0.getRow(i).createCell(++j);
						Cell cell24 = sheet0.getRow(i).createCell(++j);
						Cell cell25 = sheet0.getRow(i).createCell(++j);
						Cell cell26 = sheet0.getRow(i).createCell(++j);
						Cell cell27 = sheet0.getRow(i).createCell(++j);
						Cell cell28 = sheet0.getRow(i).createCell(++j);
						Cell cell29 = sheet0.getRow(i).createCell(++j);
						Cell cell30 = sheet0.getRow(i).createCell(++j);
						Cell cell31 = sheet0.getRow(i).createCell(++j);
						Cell cell32 = sheet0.getRow(i).createCell(++j);
						Cell cell33 = sheet0.getRow(i).createCell(++j);
						Cell cell34 = sheet0.getRow(i).createCell(++j);
						Cell cell35 = sheet0.getRow(i).createCell(++j);
						Cell cell36 = sheet0.getRow(i).createCell(++j);
						Cell cell37 = sheet0.getRow(i).createCell(++j);
						Cell cell38 = sheet0.getRow(i).createCell(++j);
						Cell cell39 = sheet0.getRow(i).createCell(++j);
						Cell cell40 = sheet0.getRow(i).createCell(++j);
						Cell cell41 = sheet0.getRow(i).createCell(++j);
						Cell cell42 = sheet0.getRow(i).createCell(++j);
						Cell cell43 = sheet0.getRow(i).createCell(++j);
						Cell cell44 = sheet0.getRow(i).createCell(++j);
						Cell cell45 = sheet0.getRow(i).createCell(++j);
						Cell cell46 = sheet0.getRow(i).createCell(++j);
						Cell cell47 = sheet0.getRow(i).createCell(++j);
						Cell cell48 = sheet0.getRow(i).createCell(++j);
						Cell cell49 = sheet0.getRow(i).createCell(++j);
						Cell cell50 = sheet0.getRow(i).createCell(++j);
						Cell cell51 = sheet0.getRow(i).createCell(++j);
						Cell cell52 = sheet0.getRow(i).createCell(++j);
						Cell cell53 = sheet0.getRow(i).createCell(++j);

						if (rec.getId() != null) {
							if (isDouble(rec.getId().toString()))
								cell0.setCellValue(Double.valueOf(rec.getId().toString()));
							else
								cell0.setCellValue(rec.getId().toString());
						} else {
							cell0.setCellValue(0);
						}
						cell1.setCellValue(rec.getFiledWithUps());
						cell2.setCellValue(rec.getCheckPayableTo());
						cell3.setCellValue(rec.getCheckSentTo());
						cell4.setCellValue(rec.getCheckSendToOther());
						cell5.setCellValue(rec.getFranchiseEmail());

						if (rec.getFranchiseNumber() != null) {
							if (isDouble(rec.getFranchiseNumber()))
								cell6.setCellValue(Double.valueOf(rec.getFranchiseNumber()));
							else
								cell6.setCellValue(rec.getFranchiseNumber());
						}
						cell7.setCellValue(rec.getFranchiseLocation());
						cell8.setCellValue(rec.getFranchiseAddress());
						cell9.setCellValue(rec.getFranchiseCity());
						cell10.setCellValue(rec.getFranchiseState());

						if (rec.getFranchiseZipCode() != null) {
							if (isDouble(rec.getFranchiseZipCode()))
								cell11.setCellValue(Double.valueOf(rec.getFranchiseZipCode()));
							else
								cell11.setCellValue(rec.getFranchiseZipCode());
						}
						cell12.setCellValue(rec.getShipperCompanyName());
						cell13.setCellValue(rec.getShipperAddress());
						cell14.setCellValue(rec.getShipperCity());
						cell15.setCellValue(rec.getShipperState());

						if (rec.getShipperZipCode() != null) {
							if (isDouble(rec.getShipperZipCode()))
								cell16.setCellValue(Double.valueOf(rec.getShipperZipCode()));
							else
								cell16.setCellValue(rec.getShipperZipCode());
						}
						cell17.setCellValue(rec.getShipperContactName());
						cell18.setCellValue(rec.getShipperTelephone());
						cell19.setCellValue(rec.getReceiverCompanyName());
						cell20.setCellValue(rec.getReceiverAddress());
						cell21.setCellValue(rec.getReceiverCity());
						cell22.setCellValue(rec.getReceiverstate());

						if (rec.getReceiverZipCode() != null) {
							if (isDouble(rec.getReceiverZipCode()))
								cell23.setCellValue(Double.valueOf(rec.getReceiverZipCode()));
							else
								cell23.setCellValue(rec.getReceiverZipCode());
						}
						cell24.setCellValue(rec.getReceiverContactName());
						cell25.setCellValue(rec.getReceiverTelephone());
						cell26.setCellValue(rec.getAirBillNo());
						cell27.setCellValue(
								rec.getDateOfShipment() != null ? sdf.format(rec.getDateOfShipment()).toString() : "");

						if (rec.getClaimAmount() != null) {
							if (isDouble(rec.getClaimAmount().toString()))
								cell28.setCellValue(Double.valueOf(rec.getClaimAmount().toString()));
							else
								cell28.setCellValue(rec.getClaimAmount().toString());
						}
						cell29.setCellValue(rec.getClaimType());
						cell30.setCellValue(rec.getProductLocation());
						cell31.setCellValue(rec.getProductCompanyName());
						cell32.setCellValue(rec.getProductAddress());
						cell33.setCellValue(rec.getProductCity());
						cell34.setCellValue(rec.getProductState());
						if (rec.getProductZipCode() != null) {
							if (isDouble(rec.getProductZipCode()))
								cell35.setCellValue(Double.valueOf(rec.getProductZipCode()));
							else
								cell35.setCellValue(rec.getProductZipCode());
						}
						cell36.setCellValue(rec.getProductContactName());
						cell37.setCellValue(rec.getProductTelephone());
						cell38.setCellValue(rec.getDescriptionIemOne());
						if (rec.getDescriptionItemOneValue() != null) {
							if (isDouble(rec.getDescriptionItemOneValue().toString()))
								cell39.setCellValue(Double.valueOf(rec.getDescriptionItemOneValue().toString()));
							else
								cell39.setCellValue(rec.getDescriptionItemOneValue().toString());
						}
						cell40.setCellValue(rec.getDescriptionItemTwo());
						if (rec.getDescriptionItemTwoValue() != null) {
							if (isDouble(rec.getDescriptionItemTwoValue().toString()))
								cell41.setCellValue(Double.valueOf(rec.getDescriptionItemTwoValue().toString()));
							else
								cell41.setCellValue(rec.getDescriptionItemTwoValue().toString());
						}
						cell42.setCellValue(rec.getDescriptionItemThree());
						if (rec.getDescriptionItemThreeValue() != null) {
							if (isDouble(rec.getDescriptionItemThreeValue().toString()))
								cell43.setCellValue(Double.valueOf(rec.getDescriptionItemThreeValue().toString()));
							else
								cell43.setCellValue(rec.getDescriptionItemThreeValue().toString());
						}

						if (rec.getTotalAmountClaimed() != null) {
							if (isDouble(rec.getTotalAmountClaimed().toString()))
								cell44.setCellValue(Double.valueOf(rec.getTotalAmountClaimed().toString()));
							else
								cell44.setCellValue(rec.getTotalAmountClaimed().toString());
						}
						cell45.setCellValue(rec.getClaimFiledBy());
						cell46.setCellValue(rec.getClaimantTelephone());
						cell47.setCellValue(rec.getClaimDateReported() != null
								? sdf.format(rec.getClaimDateReported()).toString() : "");

						if (rec.getInsuranceValue() != null) {
							if (isDouble(rec.getInsuranceValue().toString()))
								cell48.setCellValue(Double.valueOf(rec.getInsuranceValue().toString()));
							else
								cell48.setCellValue(rec.getInsuranceValue().toString());
						} else {
							cell48.setCellValue(0);
						}
						if (rec.getInsurancePremium() != null) {
							if (isDouble(rec.getInsurancePremium().toString()))
								cell49.setCellValue(Double.valueOf(rec.getInsurancePremium().toString()));
							else
								cell49.setCellValue(rec.getInsurancePremium().toString());
						} else {
							cell49.setCellValue(0);
						}
						cell50.setCellValue(rec.getCopaymentType());

						if (rec.getCopaymentValue() != null) {
							if (isDouble(rec.getCopaymentValue().toString()))
								cell51.setCellValue(Double.valueOf(rec.getCopaymentValue().toString()));
							else
								cell51.setCellValue(rec.getCopaymentValue().toString());
						} else {
							cell51.setCellValue(0);
						}
						cell52.setCellValue(rec.getActualDateOfShipment() != null
								? sdf.format(rec.getActualDateOfShipment()).toString() : "");
						if (rec.getUpsClaimNum() != null) {
							if (isDouble(rec.getUpsClaimNum()))
								cell53.setCellValue(Double.valueOf(rec.getUpsClaimNum()));
							else
								cell53.setCellValue(rec.getUpsClaimNum());
						}

						i++;
						j = -1;
						sheet0.createRow(i);
					} catch (Exception e) {
						e.printStackTrace();
						logger.error("Error in populating insurance details to Excel file." + e);
						emailService.sendFailure(InsuranceType.MARSHINSURANCEFAILURE.toString(),
								"Error in populating insurance details to Excel file.");
						return null;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in populating data to excelfile " + genFile.getName() + e);
			genFile = null;
		} finally {
			FileOutputStream fileOut = new FileOutputStream(genFile);
			book.write(fileOut);
			fileOut.flush();
			fileOut.close();
			file.close();
			genFile.setExecutable(false);

		}
		return genFile;
	}

	public static boolean isDouble(final String strInput) {
		boolean ret = true;
		try {
			Double.parseDouble(strInput);
		} catch (final NumberFormatException e) {
			e.printStackTrace();
			ret = false;
		}
		return ret;
	}

}
