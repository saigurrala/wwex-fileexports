package com.wwex.reports.model;

public enum InsuranceType {

	INSURANCECLAIMS,
	DESCREPENCYCLAIMS,
	MARSHINSURANCEFAILURE
}
