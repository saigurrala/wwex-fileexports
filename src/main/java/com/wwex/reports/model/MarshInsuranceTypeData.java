package com.wwex.reports.model;

import java.io.File;
import java.util.List;

public class MarshInsuranceTypeData {

	private String type;
	private List<MarshInsurance> marshInsuranceList;
	private File marshFile;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<MarshInsurance> getMarshInsuranceList() {
		return marshInsuranceList;
	}

	public void setMarshInsuranceList(List<MarshInsurance> marshInsuranceList) {
		this.marshInsuranceList = marshInsuranceList;
	}

	public File getMarshFile() {
		return marshFile;
	}

	public void setMarshFile(File marshFile) {
		this.marshFile = marshFile;
	}

}
