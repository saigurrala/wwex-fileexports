package com.wwex.reports.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class MarshInsurance {

	private BigDecimal id;
	private String airBillNo;
	private BigDecimal insuranceValue;
	private BigDecimal insurancePremium;
	private Timestamp airBillDate;
	private String copaymentType;
	private BigDecimal copaymentValue;
	private String filedWithUps;
	private String checkPayableTo;
	private String checkSentTo;
	private String checkSendToOther;
	private String franchiseEmail;
	private String franchiseNumber;
	private String franchiseLocation;
	private String franchiseAddress;
	private String franchiseCity;
	private String franchiseState;
	private String franchiseZipCode;
	private String shipperCompanyName;
	private String shipperAddress;
	private String shipperCity;
	private String shipperState;
	private String shipperZipCode;
	private String shipperContactName;
	private String shipperTelephone;
	private String receiverCompanyName;
	private String receiverAddress;
	private String receiverCity;
	private String receiverstate;
	private String receiverZipCode;
	private String receiverContactName;
	private String receiverTelephone;
	private String wayBillNumber;
	private Timestamp dateOfShipment;
	private BigDecimal claimAmount;
	private String claimType;
	private String productLocation;
	private String productCompanyName;
	private String productAddress;
	private String productCity;
	private String productState;
	private String productZipCode;
	private String productContactName;
	private String productTelephone;
	private String descriptionIemOne;
	private BigDecimal descriptionItemOneValue;
	private String descriptionItemTwo;
	private BigDecimal descriptionItemTwoValue;
	private String descriptionItemThree;
	private BigDecimal descriptionItemThreeValue;
	private BigDecimal totalAmountClaimed;
	private String claimFiledBy;
	private String claimantTelephone;
	private Timestamp claimDateReported;
	private String verifiedInsuranceValue;
	private String verifiedInsuranceAmount;
	private Timestamp actualDateOfShipment;
	private String upsClaimNum;

	public String getAirBillNo() {
		return airBillNo;
	}

	public void setAirBillNo(String airBillNo) {
		this.airBillNo = airBillNo;
	}

	public BigDecimal getInsuranceValue() {
		return insuranceValue;
	}

	public void setInsuranceValue(BigDecimal insuranceValue) {
		this.insuranceValue = insuranceValue;
	}

	public BigDecimal getInsurancePremium() {
		return insurancePremium;
	}

	public void setInsurancePremium(BigDecimal insurancePremium) {
		this.insurancePremium = insurancePremium;
	}

	public Timestamp getAirBillDate() {
		return airBillDate;
	}

	public void setAirBillDate(Timestamp timesStamp) {
		this.airBillDate = timesStamp;
	}

	public String getCopaymentType() {
		return copaymentType;
	}

	public void setCopaymentType(String copaymentType) {
		this.copaymentType = copaymentType;
	}

	public BigDecimal getCopaymentValue() {
		return copaymentValue;
	}

	public void setCopaymentValue(BigDecimal copaymentValue) {
		this.copaymentValue = copaymentValue;
	}

	public String getFiledWithUps() {
		return filedWithUps;
	}

	public void setFiledWithUps(String filedWithUps) {
		this.filedWithUps = filedWithUps;
	}

	public String getCheckPayableTo() {
		return checkPayableTo;
	}

	public void setCheckPayableTo(String checkPayableTo) {
		this.checkPayableTo = checkPayableTo;
	}

	public String getCheckSentTo() {
		return checkSentTo;
	}

	public void setCheckSentTo(String checkSentTo) {
		this.checkSentTo = checkSentTo;
	}

	public String getCheckSendToOther() {
		return checkSendToOther;
	}

	public void setCheckSendToOther(String checkSendToOther) {
		this.checkSendToOther = checkSendToOther;
	}

	public String getFranchiseEmail() {
		return franchiseEmail;
	}

	public void setFranchiseEmail(String franchiseEmail) {
		this.franchiseEmail = franchiseEmail;
	}

	public String getFranchiseNumber() {
		return franchiseNumber;
	}

	public void setFranchiseNumber(String franchiseNumber) {
		this.franchiseNumber = franchiseNumber;
	}

	public String getFranchiseLocation() {
		return franchiseLocation;
	}

	public void setFranchiseLocation(String franchiseLocation) {
		this.franchiseLocation = franchiseLocation;
	}

	public String getFranchiseAddress() {
		return franchiseAddress;
	}

	public void setFranchiseAddress(String franchiseAddress) {
		this.franchiseAddress = franchiseAddress;
	}

	public String getFranchiseCity() {
		return franchiseCity;
	}

	public void setFranchiseCity(String franchiseCity) {
		this.franchiseCity = franchiseCity;
	}

	public String getFranchiseState() {
		return franchiseState;
	}

	public void setFranchiseState(String franchiseState) {
		this.franchiseState = franchiseState;
	}

	public String getFranchiseZipCode() {
		return franchiseZipCode;
	}

	public void setFranchiseZipCode(String franchiseZipCode) {
		this.franchiseZipCode = franchiseZipCode;
	}

	public String getShipperCompanyName() {
		return shipperCompanyName;
	}

	public void setShipperCompanyName(String shipperCompanyName) {
		this.shipperCompanyName = shipperCompanyName;
	}

	public String getShipperAddress() {
		return shipperAddress;
	}

	public void setShipperAddress(String shipperAddress) {
		this.shipperAddress = shipperAddress;
	}

	public String getShipperCity() {
		return shipperCity;
	}

	public void setShipperCity(String shipperCity) {
		this.shipperCity = shipperCity;
	}

	public String getShipperState() {
		return shipperState;
	}

	public void setShipperState(String shipperState) {
		this.shipperState = shipperState;
	}

	public String getShipperZipCode() {
		return shipperZipCode;
	}

	public void setShipperZipCode(String shipperZipCode) {
		this.shipperZipCode = shipperZipCode;
	}

	public String getShipperContactName() {
		return shipperContactName;
	}

	public void setShipperContactName(String shipperContactName) {
		this.shipperContactName = shipperContactName;
	}

	public String getShipperTelephone() {
		return shipperTelephone;
	}

	public void setShipperTelephone(String shipperTelephone) {
		this.shipperTelephone = shipperTelephone;
	}

	public String getReceiverCompanyName() {
		return receiverCompanyName;
	}

	public void setReceiverCompanyName(String receiverCompanyName) {
		this.receiverCompanyName = receiverCompanyName;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	public String getReceiverstate() {
		return receiverstate;
	}

	public void setReceiverstate(String receiverstate) {
		this.receiverstate = receiverstate;
	}

	public String getReceiverZipCode() {
		return receiverZipCode;
	}

	public void setReceiverZipCode(String receiverZipCode) {
		this.receiverZipCode = receiverZipCode;
	}

	public String getReceiverContactName() {
		return receiverContactName;
	}

	public void setReceiverContactName(String receiverContactName) {
		this.receiverContactName = receiverContactName;
	}

	public String getReceiverTelephone() {
		return receiverTelephone;
	}

	public void setReceiverTelephone(String receiverTelephone) {
		this.receiverTelephone = receiverTelephone;
	}

	public String getWayBillNumber() {
		return wayBillNumber;
	}

	public void setWayBillNumber(String wayBillNumber) {
		this.wayBillNumber = wayBillNumber;
	}

	public Timestamp getDateOfShipment() {
		return dateOfShipment;
	}

	public void setDateOfShipment(Timestamp dateOfShipment) {
		this.dateOfShipment = dateOfShipment;
	}

	public BigDecimal getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(BigDecimal claimAmount) {
		this.claimAmount = claimAmount;
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getProductLocation() {
		return productLocation;
	}

	public void setProductLocation(String productLocation) {
		this.productLocation = productLocation;
	}

	public String getProductCompanyName() {
		return productCompanyName;
	}

	public void setProductCompanyName(String productCompanyName) {
		this.productCompanyName = productCompanyName;
	}

	public String getProductAddress() {
		return productAddress;
	}

	public void setProductAddress(String productAddress) {
		this.productAddress = productAddress;
	}

	public String getProductCity() {
		return productCity;
	}

	public void setProductCity(String productCity) {
		this.productCity = productCity;
	}

	public String getProductState() {
		return productState;
	}

	public void setProductState(String productState) {
		this.productState = productState;
	}

	public String getProductZipCode() {
		return productZipCode;
	}

	public void setProductZipCode(String productZipCode) {
		this.productZipCode = productZipCode;
	}

	public String getProductContactName() {
		return productContactName;
	}

	public void setProductContactName(String productContactName) {
		this.productContactName = productContactName;
	}

	public String getProductTelephone() {
		return productTelephone;
	}

	public void setProductTelephone(String productTelephone) {
		this.productTelephone = productTelephone;
	}

	public String getDescriptionIemOne() {
		return descriptionIemOne;
	}

	public void setDescriptionIemOne(String descriptionIemOne) {
		this.descriptionIemOne = descriptionIemOne;
	}

	public BigDecimal getDescriptionItemOneValue() {
		return descriptionItemOneValue;
	}

	public void setDescriptionItemOneValue(BigDecimal descriptionItemOneValue) {
		this.descriptionItemOneValue = descriptionItemOneValue;
	}

	public String getDescriptionItemTwo() {
		return descriptionItemTwo;
	}

	public void setDescriptionItemTwo(String descriptionItemTwo) {
		this.descriptionItemTwo = descriptionItemTwo;
	}

	public BigDecimal getDescriptionItemTwoValue() {
		return descriptionItemTwoValue;
	}

	public void setDescriptionItemTwoValue(BigDecimal descriptionItemTwoValue) {
		this.descriptionItemTwoValue = descriptionItemTwoValue;
	}

	public String getDescriptionItemThree() {
		return descriptionItemThree;
	}

	public void setDescriptionItemThree(String descriptionItemThree) {
		this.descriptionItemThree = descriptionItemThree;
	}

	public BigDecimal getDescriptionItemThreeValue() {
		return descriptionItemThreeValue;
	}

	public void setDescriptionItemThreeValue(BigDecimal descriptionItemThreeValue) {
		this.descriptionItemThreeValue = descriptionItemThreeValue;
	}

	public BigDecimal getTotalAmountClaimed() {
		return totalAmountClaimed;
	}

	public void setTotalAmountClaimed(BigDecimal bigDecimal) {
		this.totalAmountClaimed = bigDecimal;
	}

	public String getClaimFiledBy() {
		return claimFiledBy;
	}

	public void setClaimFiledBy(String claimFiledBy) {
		this.claimFiledBy = claimFiledBy;
	}

	public String getClaimantTelephone() {
		return claimantTelephone;
	}

	public void setClaimantTelephone(String claimantTelephone) {
		this.claimantTelephone = claimantTelephone;
	}

	public Timestamp getClaimDateReported() {
		return claimDateReported;
	}

	public void setClaimDateReported(Timestamp claimDateReported) {
		this.claimDateReported = claimDateReported;
	}

	public String getVerifiedInsuranceValue() {
		return verifiedInsuranceValue;
	}

	public void setVerifiedInsuranceValue(String verifiedInsuranceValue) {
		this.verifiedInsuranceValue = verifiedInsuranceValue;
	}

	public String getVerifiedInsuranceAmount() {
		return verifiedInsuranceAmount;
	}

	public void setVerifiedInsuranceAmount(String verifiedInsuranceAmount) {
		this.verifiedInsuranceAmount = verifiedInsuranceAmount;
	}

	public String getUpsClaimNum() {
		return upsClaimNum;
	}

	public void setUpsClaimNum(String upsClaimNum) {
		this.upsClaimNum = upsClaimNum;
	}

	public Timestamp getActualDateOfShipment() {
		return actualDateOfShipment;
	}

	public void setActualDateOfShipment(Timestamp actualDateOfShipment) {
		this.actualDateOfShipment = actualDateOfShipment;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}
