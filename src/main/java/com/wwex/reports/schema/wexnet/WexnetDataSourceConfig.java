package com.wwex.reports.schema.wexnet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author blake
 * @since 4/24/15
 */
@Configuration
@ComponentScan
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "wexnetEntityManagerFactory", transactionManagerRef = "wexnetTransactionManager")
public class WexnetDataSourceConfig {

	private static final Logger logger = LoggerFactory.getLogger(WexnetDataSourceConfig.class);

	@Value("${spring.datasource.wexnet.schema}")
	private String dbSchema;

	@Value("${spring.datasource.wexnet.ddl_mode}")
	private String dbDdlMode;

	@Value("${spring.datasource.wexnet.show_stats:false}")
	private boolean showStats;

	@Value("${spring.datasource.wexnet.show_sql:false}")
	private boolean showSql;

	@Value("${spring.datasource.wexnet.import_files}")
	private String dbInitFiles;

	@Value("${spring.datasource.wexnet.driverClassName}")
	private String dbDriverClassName;
	
	
	
	

	@Bean(name = "dsWexnet")
	@ConfigurationProperties(prefix = "spring.datasource.wexnet")
	public DataSource wwexDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "jdbcWexnet")
	@Autowired
	public JdbcTemplate masterJdbcTemplate(@Qualifier("dsWexnet") DataSource dataSource) {

		return new JdbcTemplate(dataSource);
	}

	@Bean(name = "wexnetEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean wexnetEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", dbDdlMode);
		properties.put("hibernate.generate_statistics", showStats);
		properties.put("hibernate.show_sql", showSql);
		properties.put("hibernate.format_sql", false);
		properties.put("hibernate.enable_lazy_load_no_trans", true);
		properties.put("hibernate.default_schema", dbSchema);
		if (!StringUtils.isEmpty(dbInitFiles)) {
			properties.put("hibernate.hbm2ddl.import_files", dbInitFiles);
		}
		properties.put("hibernate.hbm2ddl.import_files_sql_extractor",
				"org.hibernate.tool.hbm2ddl.MultipleLinesSqlCommandExtractor");
		properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		properties.put("hibernate.implicit_naming_strategy",
				"org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
		properties.put("hibernate.physical_naming_strategy",
				"org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
		properties.put("hibernate.ejb.entitymanager_factory_name", "wexnetEntityManagerFactory");
		logger.info(properties.toString());
		return builder.dataSource(wwexDataSource()).packages("com.wwex.reports.service")
				.persistenceUnit("wexnetPersistenceUnit").properties(properties).build();
	}

	@Bean(name = "wexnetTransactionManager")
	public PlatformTransactionManager wexnetTransactionManager(
			@Qualifier("wexnetEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
		jpaTransactionManager.setPersistenceUnitName("wexnetPersistenceUnit");
		return jpaTransactionManager;
	}
}
