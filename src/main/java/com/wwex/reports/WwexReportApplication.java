package com.wwex.reports;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.wwex.reports.schema.wexnet.WexnetDataSourceConfig;

@SpringBootApplication
@EnableScheduling
@Import({

		WexnetDataSourceConfig.class })
public class WwexReportApplication {
	static final Logger logger = Logger.getLogger(WwexReportApplication.class);

	public static void main(String[] args) {

		logger.info("Starting Reporting Application");
		SpringApplication.run(WwexReportApplication.class, args);
	}

}
