package com.wwex.reports.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import com.wwex.reports.model.MarshInsurance;
import com.wwex.reports.model.MarshInsuranceTypeData;

public class TestExcelFileGenerationService {

	@Test
	public void generateMarshInsuranceFile() {
		MarshInsuranceTypeData marshInsuranceTypeData = new MarshInsuranceTypeData();
		marshInsuranceTypeData.setType("Ins");
		Assert.assertEquals("Ins", marshInsuranceTypeData.getType());
	}

	public File populateDataToExcel(FileInputStream file, File genFile, List<MarshInsurance> excelFileData) {

		try {
			HSSFWorkbook book = new HSSFWorkbook(file);
			HSSFSheet sheet0 = book.getSheetAt(0);
			int i = 1, j = -1;
			if (!CollectionUtils.isEmpty(excelFileData)) {
				for (MarshInsurance rec : excelFileData) {
					Cell cell0 = sheet0.getRow(i).createCell(++j);
					Cell cell1 = sheet0.getRow(i).createCell(++j);
					Cell cell2 = sheet0.getRow(i).createCell(++j);
					Cell cell3 = sheet0.getRow(i).createCell(++j);
					Cell cell4 = sheet0.getRow(i).createCell(++j);
					Cell cell5 = sheet0.getRow(i).createCell(++j);
					Cell cell6 = sheet0.getRow(i).createCell(++j);
					Cell cell7 = sheet0.getRow(i).createCell(++j);
					Cell cell8 = sheet0.getRow(i).createCell(++j);
					Cell cell9 = sheet0.getRow(i).createCell(++j);
					Cell cell10 = sheet0.getRow(i).createCell(++j);
					Cell cell11 = sheet0.getRow(i).createCell(++j);
					Cell cell12 = sheet0.getRow(i).createCell(++j);
					Cell cell13 = sheet0.getRow(i).createCell(++j);
					Cell cell14 = sheet0.getRow(i).createCell(++j);
					Cell cell15 = sheet0.getRow(i).createCell(++j);
					Cell cell16 = sheet0.getRow(i).createCell(++j);
					Cell cell17 = sheet0.getRow(i).createCell(++j);
					Cell cell18 = sheet0.getRow(i).createCell(++j);
					Cell cell19 = sheet0.getRow(i).createCell(++j);
					Cell cell20 = sheet0.getRow(i).createCell(++j);
					Cell cell21 = sheet0.getRow(i).createCell(++j);
					Cell cell22 = sheet0.getRow(i).createCell(++j);
					Cell cell23 = sheet0.getRow(i).createCell(++j);
					Cell cell24 = sheet0.getRow(i).createCell(++j);
					Cell cell25 = sheet0.getRow(i).createCell(++j);
					Cell cell26 = sheet0.getRow(i).createCell(++j);
					Cell cell27 = sheet0.getRow(i).createCell(++j);
					Cell cell28 = sheet0.getRow(i).createCell(++j);
					Cell cell29 = sheet0.getRow(i).createCell(++j);
					Cell cell30 = sheet0.getRow(i).createCell(++j);
					Cell cell31 = sheet0.getRow(i).createCell(++j);
					Cell cell32 = sheet0.getRow(i).createCell(++j);
					Cell cell33 = sheet0.getRow(i).createCell(++j);
					Cell cell34 = sheet0.getRow(i).createCell(++j);
					Cell cell35 = sheet0.getRow(i).createCell(++j);
					Cell cell36 = sheet0.getRow(i).createCell(++j);
					Cell cell37 = sheet0.getRow(i).createCell(++j);
					Cell cell38 = sheet0.getRow(i).createCell(++j);
					Cell cell39 = sheet0.getRow(i).createCell(++j);
					Cell cell40 = sheet0.getRow(i).createCell(++j);
					Cell cell41 = sheet0.getRow(i).createCell(++j);
					Cell cell42 = sheet0.getRow(i).createCell(++j);
					Cell cell43 = sheet0.getRow(i).createCell(++j);
					Cell cell44 = sheet0.getRow(i).createCell(++j);
					Cell cell45 = sheet0.getRow(i).createCell(++j);
					Cell cell46 = sheet0.getRow(i).createCell(++j);
					Cell cell47 = sheet0.getRow(i).createCell(++j);
					Cell cell48 = sheet0.getRow(i).createCell(++j);
					Cell cell49 = sheet0.getRow(i).createCell(++j);
					Cell cell50 = sheet0.getRow(i).createCell(++j);
					Cell cell51 = sheet0.getRow(i).createCell(++j);
					Cell cell52 = sheet0.getRow(i).createCell(++j);
					Cell cell53 = sheet0.getRow(i).createCell(++j);

					cell0.setCellValue(rec.getId().toString());
					cell1.setCellValue(rec.getFiledWithUps());
					cell2.setCellValue(rec.getCheckPayableTo());
					cell3.setCellValue(rec.getCheckSentTo());
					cell4.setCellValue(rec.getCheckSendToOther());
					cell5.setCellValue(rec.getFranchiseEmail());
					cell6.setCellValue(rec.getFranchiseNumber());
					cell7.setCellValue(rec.getFranchiseLocation());
					cell8.setCellValue(rec.getFranchiseAddress());
					cell9.setCellValue(rec.getFranchiseCity());
					cell10.setCellValue(rec.getFranchiseState());
					cell11.setCellValue(rec.getFranchiseZipCode());
					cell12.setCellValue(rec.getShipperCompanyName());
					cell13.setCellValue(rec.getShipperAddress());
					cell14.setCellValue(rec.getShipperCity());
					cell15.setCellValue(rec.getShipperState());
					cell16.setCellValue(rec.getShipperZipCode());
					cell17.setCellValue(rec.getShipperContactName());
					cell18.setCellValue(rec.getShipperTelephone());
					cell19.setCellValue(rec.getReceiverCompanyName());
					cell20.setCellValue(rec.getReceiverAddress());
					cell21.setCellValue(rec.getReceiverCity());
					cell22.setCellValue(rec.getReceiverstate());
					cell23.setCellValue(rec.getReceiverZipCode());
					cell24.setCellValue(rec.getReceiverContactName());
					cell25.setCellValue(rec.getReceiverTelephone());
					cell26.setCellValue(rec.getWayBillNumber());
					cell27.setCellValue(rec.getDateOfShipment() != null ? rec.getDateOfShipment().toString() : "");
					cell28.setCellValue(rec.getClaimAmount() != null ? rec.getClaimAmount().toString() : "");
					cell29.setCellValue(rec.getClaimType());
					cell30.setCellValue(rec.getProductLocation());
					cell31.setCellValue(rec.getProductCompanyName());
					cell32.setCellValue(rec.getProductAddress());
					cell33.setCellValue(rec.getProductCity());
					cell34.setCellValue(rec.getProductState());
					cell35.setCellValue(rec.getProductZipCode());
					cell36.setCellValue(rec.getProductContactName());
					cell37.setCellValue(rec.getProductTelephone());
					cell38.setCellValue(rec.getDescriptionIemOne());
					cell39.setCellValue(rec.getDescriptionItemOneValue() != null
							? rec.getDescriptionItemOneValue().toString() : "");
					cell40.setCellValue(rec.getDescriptionItemTwo());
					cell41.setCellValue(rec.getDescriptionItemTwoValue() != null
							? rec.getDescriptionItemTwoValue().toString() : "");
					cell42.setCellValue(rec.getDescriptionItemThree());
					cell43.setCellValue(rec.getDescriptionItemThreeValue() != null
							? rec.getDescriptionItemThreeValue().toString() : "");
					cell44.setCellValue(
							rec.getTotalAmountClaimed() != null ? rec.getTotalAmountClaimed().toString() : "");
					cell45.setCellValue(rec.getClaimFiledBy());
					cell46.setCellValue(rec.getClaimantTelephone());
					cell47.setCellValue(
							rec.getClaimDateReported() != null ? rec.getClaimDateReported().toString() : "");
					cell48.setCellValue(rec.getInsuranceValue() != null ? rec.getInsuranceValue().toString() : "");
					cell49.setCellValue(rec.getInsurancePremium() != null ? rec.getInsurancePremium().toString() : "");
					cell50.setCellValue(rec.getCopaymentType());
					cell51.setCellValue(rec.getCopaymentValue() != null ? rec.getCopaymentValue().toString() : "");
					cell52.setCellValue(
							rec.getActualDateOfShipment() != null ? rec.getActualDateOfShipment().toString() : "");
					cell53.setCellValue(rec.getUpsClaimNum());

					i++;
					j = -1;
					sheet0.createRow(i);
				}
			}
			FileOutputStream fileOut = new FileOutputStream(genFile);
			book.write(fileOut);
			fileOut.flush();
			fileOut.close();
			file.close();
			genFile.setExecutable(false);
		} catch (Exception e) {
		}
		return genFile;
	}

}
